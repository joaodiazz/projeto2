antes de sair o professor organizou a sala em um grande 
"jogo da velha", dividindo os 36 alunos em 3 fileiras 
com 3 grupos de 4 pessoas.

em cada fileira um dos grupos deveria ter 3 alunos de
Portugues e 1 aluno de Matematica, o outro grupo da mesma
fileira deveria de 3 alunos de Matematica e 1 de
Portugues e o outro grupo da mesma fileira deveria ter 2
alunos de Matematica e 2 alunos de Portugues. seguindo o
mesma padrão com as outras fileiras.
 (como esta na ilustração 2)

como o professor já tinha dividido tudo. para ele achar o
aluno errado, ele começou a checar se todas as fileiras 
estavam certas, com isso ele descobriu que na terceira 
fileira um dos grupos que antes tinha 3 alunos de Matematica 
e 1 de Portugues, agora tinha 2 alunos de Portugues e 2 
alunos de Matematica quebrando o padrão que ele tinha
estabelecido. achado assim o aluno que trocou de materia.